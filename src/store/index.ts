import { createStore } from 'vuex'

interface IStore {
  count: number;
  user?: string;
  cart: { product: string, quantity: number, price: number }[]
}

const store = createStore({
  state(): IStore {
    return {
      count: 0,
      user: undefined,
      cart: []
    }
  },
  mutations: {
    initialiseStore(state) {
      if (localStorage.getItem('store')) {
        this.replaceState(
          Object.assign(state, JSON.parse(localStorage.getItem('store') as string))
        );
      }
    },
    increment(state) {
      state.count++
    },
    decrement(state) {
      state.count--;
    },
    resetToZero(state) {
      state.count = 0;
    },
    login(state, data) {
      state.user = data.username;
    },
    logout(state) {
      state.user = undefined;
    },
    addToCart(state, data) {
      const currentRowIndex = state.cart.findIndex(row => row.product === data.product);

      if (currentRowIndex !== -1) {
        state.cart[currentRowIndex].quantity += data.quantity;
      } else {
        state.cart.push({ product: data.product, quantity: data.quantity, price: data.price });
      }
    },
    clearAllCart(state) {
      state.cart = [];
    }
  }
});

store.subscribe((mutation, state) => {
  localStorage.setItem('store', JSON.stringify(state));
});

export default store;
