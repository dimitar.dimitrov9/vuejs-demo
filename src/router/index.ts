import * as Vue from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import EditPost from '../views/EditPost.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: { title: 'home page' }
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue'),
      meta: { title: 'about page' }
    },
    {
      path: '/posts',
      name: 'posts',
      component: () => import('../views/PostsView.vue'),
      meta: { title: 'posts page' }
    },
    {
      path: '/post/:id',
      name: 'post',
      component: () => import('../views/PostView.vue'),
      meta: { title: 'post page {param.id}' },
      children: [
        {
          path: 'edit',
          component: EditPost
        }
      ]
    },
    {
      path: '/demo',
      name: 'demo',
      component: () => import('../views/DemoView.vue'),
      meta: { title: 'demo page' }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue'),
      meta: { title: 'login page' }
    },
    {
      path: '/products',
      name: 'products',
      component: () => import('../views/ProductsView.vue'),
      meta: { title: 'products page' }
    },
    {
      path: '/product/:id',
      name: 'product',
      component: () => import('../views/ProductView.vue'),
      meta: { title: 'product page {param.id}' }
    },
    {
      path: '/cart',
      name: 'cart',
      component: () => import('../views/CartView.vue'),
      meta: { title: 'cart' }
    },
    {
      path: '/:catchAll(.*)',
      name: 'not-found',
      component: () => import('../views/PageNotFound.vue'),
      meta: { title: '404 page' }
    },
  ]
});

router.afterEach(to => {
  Vue.nextTick(() => {
    let title = to.meta.title || 'VUE.JS DEMO APPLICATION';

    Object.entries(to.params).forEach(([k, v]) => {
      while (title.includes(`{param.${k}}`)) {
        title = title.replace(`{param.${k}}`, v);
      }
    });

    document.title = title;
  });
});


export default router
